# Zero

Cryptography programming language

## Features

 * Asymmetric algorithms
 * Symmetric algorithms
 * Hash algorithms
 * Proofs of knowledge
 * Proofs
 * Simple syntax
 * Easy to integrate with
 * Multiparty computation
 * Simulations
 * Write and export libraries
 * Signature of proofs (need a CA) 
    * maybe code is sent to cloud to get real signature
 * Compiled to C
 * Generate written proof
 * Create structures
 * Assign public/secret attributes
 * modular arithmetic
 * declarative model, rather than imperative

## Editor

 * syntax highlighting
 * transform symbols
 * <-     : ←
 * Z      : ℤ
 * =      : ≡
 * in     : ∈
 * not in : ∉

## Examples

### RSA

```
#import Primes

init():
    private p <- Primes
    private q <- Primes
    private t = lcm(p-1, q-1) 
    public  n = p*q
    public  e <- 1 < e < t and gcd(e, t) == 1
    private d = 1/e mod t


encrypt(m):
    yield m^e mod n

decrypt(c):
    yield c^d mod n

keypair():
    yield e, n
    
```


### Compiling

    ./0 rsa.0

### Testing
    ./0 --prove rsa.0

